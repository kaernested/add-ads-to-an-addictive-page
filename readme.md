# Create ads

Create ads to sell or request items.

## Getting Started

To add to the project you should fork it to your repository where you can then clone it.

### Prerequisites

You will need node.js, yarn and jest.

To install yarn you write this command in your terminal:

```
npm install -g yarn
```

After installing yarn, you van install Jest. Jest is a testing framework. To install you write this command in your terimnal:

```
yarn add --dev jest
```

### Open the page in your browser

After forking this project to your GitLab repository you can open the page in your browser. You simply go under settings > pages and there you can open the link to this project.

## Contributing

To contribute to this project you must fork it to your repository. After doing so you can add to the main.js file. To add a new ad you must copy this code and paste it above the first ad. Then you can fill out the information and add an image.

```
// NEW ADD

    document.querySelector('#main').innerHTML += addHeader(
        '<h2>HEADER</h2>'
    );
    document.querySelector('#main').innerHTML += addImg(
        '<img src="#">'
    );
    document.querySelector('#main').innerHTML += addAd(
        '<p>CONTENT</p>'
    );
```

Now you have added a new ad!

## Running the tests

Before you commit your code and request a merge you should test your code. Simply write 

```
yarn test
```

into your terminal. 

If everything is OK it should look somthing like this:

```
Test Suites: 1 passed, 1 total
Tests:       3 passed, 3 total
Snapshots:   0 total
Time:        2.651s
Ran all test suites.
✨  Done in 5.20s.
```
