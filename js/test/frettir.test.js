document.body.innerHTML = '<div id="ad"></div>'

var { addHeader, addImg, addAd } = require('../main.js')

test('The news item should contain a <h2> header', function (){
    expect( addHeader() ).toMatch(/<h2>/);
});

test('The news item should contain an <img> element', function (){
    expect( addImg() ).toMatch(/<img/);
});

test('The news item should contain a <p> element', function (){
    expect( addAd() ).toMatch(/<p>/);
});
